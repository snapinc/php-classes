<?php

/*
SAMPLE USAGE:
$curl = new SnapBasecamp(ACCOUNT_ID, USERNAME, PASSWORD);
$curl->get('/projects.json')->result();
*/

require_once('SnapCurl.php');

Class SnapBasecamp extends SnapCurl {

	private $account_id;
	private $username;
	private $password;

	public function __construct($account_id = null, $username = null, $password = null) {

		parent::__construct();
		$this->account_id = $account_id;
		$this->username = $username;
		$this->password = $password;

		// Basecamp Next returns data in JSON format, so this is set by default
		$this->set_header('Content-Type', 'application/json; charset=utf-8');

		// Authorize using the passed in username and password
		$this->authorize();

	}

	public function url($url = null) {

		// Add the correct Account ID and URL slug
		$this->url = 'https://basecamp.com/'.$this->account_id.'/api/v1'.$url;

		return $this;

	}

	private function authorize() {

		// For authorization we use Basic, setting curl option for USERPWD
		$this->set_option('HTTPAUTH', CURLAUTH_BASIC);
		$this->set_option('USERPWD', $this->username.':'.$this->password);

	}

}