<?php

/*
SAMPLE USAGE:
$curl = new SnapTrello(API_KEY, TOKEN);
$curl->get('/members/my/boards')->json_as_array();
*/

require_once('SnapCurl.php');

Class SnapTrello extends SnapCurl {

	private $api_key;
	private $token;

	public function __construct($api_key = null, $token = null) {

		parent::__construct();
		$this->api_key = $api_key;
		$this->token = $token;

		// Trello returns information in JSON format
		$this->set_header('Content-Type', 'application/json; charset=utf-8');

	}

	// Override the default URL function to use the right Trello syntax
	public function url($url = null) {

		// Append the URL slug, API Key, and Token
		$this->url = 'https://api.trello.com/1'.$url.'?key='.$this->api_key.'&token='.$this->token;

		return $this;

	}

}