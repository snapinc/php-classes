<?php

/*
SAMPLE USAGE:
$curl = new SnapCurl();
$curl->get('http://xxxxxxxxxx')->result();
$curl->get('http://xxxxxxxxxx')->json_as_array();
$curl->get('http://xxxxxxxxxx')->json_as_object();
$curl->post('http://xxxxxxxxx', array('name'=>'John Smith'))->info('http_code');
*/

class SnapCurl {

	private $connection;
	protected $url;
	private $options;
	private $headers;
	private $params;

	public function __construct() {

		// INITIALIZE THE VARIABLES
		$this->url = null;
		$this->options = array();
		$this->headers = array();
		$this->params = array();

	}

	// CREATE A CURL HANDLER
	private function open() {

		// OPEN CURL CONNECTION
		$this->connection = curl_init();

		// SET DEFAULT VALUES
		$this->set_defaults();

	}

	// SETS DEFAULT VALUES FOR CURL (CAN BE OVERRIDDEN)
	private function set_defaults() {

		// SET DEFAULT OPTIONS
		$this->set_option(array(
			'RETURNTRANSFER' 	=> 1,
			'HEADER'			=> 1,
			'TIMEOUT'			=> 10
		));

		// SET USER AGENT HEADER (the browser's, if avail, otherwise custom)
		$tmp_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? 'SnapCurl (http://snapshotgroup.com)' : $_SERVER['HTTP_USER_AGENT'];

	}

	// Close CURL handler
	private function close() {

		curl_close($this->connection);

	}

	// Set the URL
	public function url($url = null) {

		$this->url = $url;

		return $this;

	}

	// Run the CURL request
	private function request($type = null, $url = null, $params = null, $params_are_raw = false) {

		// Allowing to set URL this way
		if (!empty($url)) {
			$this->url($url);
		}

		// Allowing to set parameters this way
		if (!empty($params)) {
			$this->set_param($params);
		}

		// Normalize TYPE
		$type = strtoupper($type);

		// Open a CURL handler
		$this->open();

		// GET PARAMS TO SEND
		$tmp_params = empty($this->params) ? null : ($params_are_raw ? $params : json_encode($this->params));

		// DO TYPE-SPECIFIC ACTIONS
		switch ($type) {
			case 'POST':
				$this->set_option('POST', 1);
				$this->set_option('POSTFIELDS', $tmp_params);
				break;
			case 'GET':
				$this->set_option('HTTPGET', 1);
				if (!empty($tmp_params)) {
					$this->url = $this->url.'&'.http_build_query($tmp_params);
				}
				break;
			case 'PUT':
				$this->set_option('CUSTOMREQUEST', 'PUT');
				if (!empty($tmp_params)) {
					$this->set_option('POSTFIELDS', $tmp_params);
				}
				break;
			case 'DELETE':
				$this->set_option('CUSTOMREQUEST', 'DELETE');
				break;
		}

		// APPLY HEADERS
		$final_headers = array();
		foreach ($this->headers AS $key => $value) {
			$final_headers[] = $key.': '.$value;
		}
		$this->set_option('HTTPHEADER', $final_headers);

		// APPLY URL
		$this->set_option('URL', $this->url);

		// APPLY OPTIONS
		foreach ($this->options AS $option => $value) {
			curl_setopt($this->connection, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option))), $value);
		}

		// SUBMIT REQUEST
		$result = curl_exec($this->connection);

		// ERROR? SAY SO.
		if (curl_errno($this->connection)) {
			die('CURL ERROR ('.curl_errno($this->connection).'): '.curl_error($this->connection));
		}

		// GET RESPONSE OBJECT
		$response = new SnapCurlResponse($this->connection, $result);

		// RETURN THE RESPONSE
		return $response;

	}

	// Set a single header (key/value), or an array of headers (using first param)
	public function set_header($key = null, $value = null) {

		// IF PASSING IN AN ARRAY OF HEADERS
		if (is_array($key)) {
			// MERGE THEM WITH EXISTING
			$this->headers = array_merge($this->headers, $key);
		}
		// ELSE IF PASSING IN SINGLE HEADER
		else {
			// ADD IT TO ARRAY
			$this->headers[$key] = $value;
		}

		return $this;

	}

	// Set a single option (key/value), or an array of options (using first param)
	public function set_option($option = null, $value = null) {

		// IF PASSING IN AN ARRAY OF OPTIONS
		if (is_array($option)) {
			// MERGE THEM WITH EXISTING
			$this->options = array_merge($this->options, $option);
		}
		// ELSE IF PASSING IN SINGLE OPTION
		else {
			// ADD IT TO ARRAY
			$this->options[$option] = $value;
		}

		return $this;

	}

	// Set a single param (key/value), or an array of params (using first param)
	public function set_param($key = null, $value = null) {

		// IF PASSING IN AN ARRAY OF PARAMS
		if (is_array($key)) {
			// MERGE THEM WITH EXISTING
			$this->params = array_merge($this->params, $key);
		}
		// ELSE IF PASSING IN SINGLE OPTION
		else {
			// ADD IT TO ARRAY
			$this->params[$key] = $value;
		}

		return $this;

	}

	// Run a GET request (url and params optional)
	public function get($url = null, $params = array()) {

		return $this->request('get', $url, $params);

	}

	// Run a PUT request (url and params optional)
	public function put($url = null, $params = array(), $params_are_raw = false) {

		return $this->request('put', $url, $params, $params_are_raw);

	}

	// Run a POST request (url and params optional)
	public function post($url = null, $params = array()) {

		return $this->request('post', $url, $params);

	}

	// Run a DELETE request (url and params optional)
	public function delete($url = null, $params = array()) {

		return $this->request('delete', $url, $params);

	}

}

class SnapCurlResponse {

	private $info;
	private $headers;
	private $body;

	public function __construct($connection = null, $result = null) {

		// Reset variables
		$this->info = array();
		$this->headers = false;
		$this->body = false;

		// Get information about the handler
		$this->info = curl_getinfo($connection);

		// Get header size
		$tmp_header_size = $this->info['header_size'];

		// Separate the header from the result, if a header exists
		if (!empty($tmp_header_size)) {
			$this->headers = mb_substr($result, 0, $tmp_header_size);
			$this->body = mb_substr($result, $tmp_header_size);
		} else {
			$this->body = $result;
		}

		// Return the Response Obj
		return $this;

	}

	// If you try to echo the response obj, this will output the body
	public function __toString() {

		return $this->body;

	}

	// Just in case you want to know what an http code means
	public function convert_http_code($code = null) {
		$http_codes = array(
			200 => "OK",
			201 => "CREATED",
			202 => "ACCEPTED",
			203 => "PARTIAL INFORMATION",
			204 => "NO RESPONSE",
			400 => "BAD REQUEST",
			401 => "UNAUTHORIZED",
			402 => "PAYMENTREQUIRED",
			403 => "FORBIDDEN",
			404 => "NOT FOUND",
			500 => "INTERNAL ERROR",
			501 => "NOT IMPLEMENTED",
			502 => "SERVICE TEMPORARILY OVERLOADED",
			503 => "GATEWAY TIMEOUT",
			301 => "MOVED",
			302 => "FOUND",
			303 => "METHOD",
			304 => "NOT MODIFIED"
		);
		return empty($code) || empty($http_codes[$code]) ? false : $http_codes[$code];
	}

	// Returns a specific header from the response
	public function get_header($key = null) {

		$return_val = false;

		if (is_null($key)) {
			$return_val = $this->headers;
		}
		else {
			$return_val = empty($this->headers[$key]) ? false : $this->headers[$key];
		}

		return $return_val;

	}

	// Returns a JSON response as an array
	public function json_as_array() {

		return json_decode($this->body, true);

	}

	// Returns a JSON response as an object
	public function json_as_object() {

		return json_decode($this->body, false);

	}

	// Returns the response body
	public function result() {

		return $this->body;

	}

	// Returns information about the CURL handler request
	public function info($key = null) {
		return is_null($key) ? $this->info : (empty($this->info[$key])?false:$this->info[$key]);
	}

}
